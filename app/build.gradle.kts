
plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    kotlin("kapt")
    id("com.google.gms.google-services")
    id("androidx.navigation.safeargs.kotlin")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdkVersion(Versions.androidCompileSdkVersion)
    buildToolsVersion = Versions.androidBuildToolsVersion

    defaultConfig {
        applicationId = "com.dynamicforma.app"
        minSdkVersion(Versions.androidMinSdkVersion)
        targetSdkVersion(Versions.androidTargetSdkVersion)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables.useSupportLibrary = true
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        dataBinding = true
    }

}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Dependencies.kotlin)
    implementation(Dependencies.androidCompat)
    implementation(Dependencies.androidCore)
    implementation(Dependencies.constraintLayout)
    implementation(Dependencies.material)

    //    ANDROID ARCHITECTURE COMPONENETS
    implementation(Dependencies.paging)
    implementation(Dependencies.lifecycleExt)
    implementation(Dependencies.lifecycleViewModel)
    implementation(Dependencies.lifecycleLiveData)
    implementation(Dependencies.lifecycleReactivestreams)
    implementation(Dependencies.lifecycleCommon)
    implementation(Dependencies.navigationFragment)
    implementation(Dependencies.navigationUi)
    implementation(Dependencies.preferences)

    //    DAGGER
    implementation(Dependencies.hilt)
    implementation(Dependencies.hiltViewModel)
    kapt(Dependencies.hiltCompiler)
    kapt(Dependencies.hiltAndroidCompiler)
    implementation(Dependencies.legacySupport)

    //    FIRESTORE
    implementation(Dependencies.firebaseCore)
    implementation(Dependencies.firestore)
    implementation(Dependencies.firebaseAuth)

    // RX
    implementation(Dependencies.rxAndroid)
    implementation(Dependencies.rxKotlin)
    implementation(Dependencies.rxJava)

    //  TEST
    testImplementation(Dependencies.junit)
    androidTestImplementation(Dependencies.androidTestRunner)
    androidTestImplementation(Dependencies.espressoCore)

    // TIMBER
    implementation(Dependencies.timber)
}
