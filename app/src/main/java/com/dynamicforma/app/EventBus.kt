package com.dynamicforma.app

import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.processors.PublishProcessor
import io.reactivex.rxkotlin.ofType
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Events are posted to common bus and all active subscribers are notified of changes. Threading and
 * backpresssure can be handled by subscribers.
 */
interface EventBus {
    /**
     * Post an event to bus.
     */
    fun postEvent(event: Any)

    /**
     * @return A [Flowable] that will emit event for every [postEvent] call until subscriber is
     * unsubscribed.
     *
     * Note: This does not emit event on any particular [Scheduler]
     */
    fun observeEvents(): Flowable<Any>

    /**
     * Same as [observeEvent] but emits event on the UI thread.
     */
    fun observeEventsOnUi(): Flowable<Any>
}


/**
 * Observer extension that will only emit event of type [T], filtering all other events.
 */
inline fun <reified T : Any> EventBus.observeEvent(): Flowable<T> {
    return observeEvents().ofType()
}

/**
 * Same as [observeEvent] but emits on UI thread.
 */
inline fun <reified T : Any> EventBus.observeEventOnUi(): Flowable<T> {
    return observeEventsOnUi().ofType()
}

/**
 * [EventBus] implementation backed by a [PublishProcessor]
 */

object RxBus : EventBus {
    private val publishProcessor = PublishProcessor.create<Any>()

    override fun postEvent(event: Any) {
        publishProcessor.onNext(event)
    }

    override fun observeEvents(): Flowable<Any> {
        return publishProcessor.serialize()
    }

    override fun observeEventsOnUi(): Flowable<Any> {
        return observeEvents()
            .observeOn(AndroidSchedulers.mainThread())
    }
}