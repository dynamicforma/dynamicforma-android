package com.dynamicforma.app.ui.main

import com.dynamicforma.app.BR
import com.dynamicforma.app.R
import com.dynamicforma.app.data.entities.FilledForm
import com.dynamicforma.app.databinding.FilledFormItemBinding
import com.dynamicforma.app.view.common.ItemListener
import com.dynamicforma.app.view.common.AnyListAdapter

class FilledFormListAdapter(
    clickListener: ItemListener<FilledForm>
) : AnyListAdapter<FilledForm, FilledFormItemBinding>(R.layout.filled_form_item, BR.filledForm, clickListener)