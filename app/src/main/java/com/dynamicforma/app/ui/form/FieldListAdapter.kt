package com.dynamicforma.app.ui.form


import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.setMargins
import androidx.recyclerview.widget.RecyclerView
import com.dynamicforma.app.R
import com.dynamicforma.app.data.entities.FilledField
import com.dynamicforma.app.view.common.AutoUpdatableAdapter
import com.dynamicforma.app.view.field.DynamicField
import com.dynamicforma.app.view.field.IDynamicField
import kotlin.properties.Delegates


class FieldListAdapter : RecyclerView.Adapter<FieldListAdapter.FieldViewHolder>(), AutoUpdatableAdapter {

    var items: List<FilledField> by Delegates.observable(emptyList()){_, oldValue, newValue ->
        autoNotify(oldValue, newValue) { old, new -> old.id == new.id}
    }

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = FieldViewHolder(
        CardView(parent.context),
        viewType
    )

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: FieldViewHolder, position: Int) {
        holder.dynamicField.bindTo(items[position])
    }

    override fun getItemViewType(position: Int): Int = when {
        items.isNotEmpty()-> items[position].fieldType
        else -> -1
    }

    override fun getItemId(position: Int) = items[position].id.hashCode().toLong()

    inner class FieldViewHolder(val card: CardView, type : Int) : RecyclerView.ViewHolder(card) {
        val dynamicField =  DynamicField(card.context, type = type)
        init {
            card.layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
//                .apply {
//                val dpAsPixels = card.resources.getDimensionPixelSize(R.dimen.item_margin)
////                this.setMargins(dpAsPixels)
//                this.setMargins(dpAsPixels, 0, dpAsPixels, 0)
//            }

            card.cardElevation = 10.0f
            card.radius = 10.0f
            card.addView(dynamicField)
        }
    }
}