package com.dynamicforma.app.ui.main


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dynamicforma.app.EventFormSaved
import com.dynamicforma.app.R
import com.dynamicforma.app.RxBus
import com.dynamicforma.app.data.entities.FilledForm
import com.dynamicforma.app.observeEventOnUi
import com.dynamicforma.app.utils.AuthenticationState
import com.dynamicforma.app.view.common.ItemListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class MainFragment : Fragment() {

    @Inject lateinit var auth: FirebaseAuth
    private lateinit var recyclerForms: RecyclerView
    private val formsAdapter = FilledFormListAdapter(ItemListener { openForm(it) }).apply { setHasStableIds(true) }
    private lateinit var tvEmpty: View
    private val viewModel: MainViewModel by viewModels()
    private lateinit var progressBar: ProgressBar
    private lateinit var root: View
    private val compositeDisposable = CompositeDisposable()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        root = inflater.inflate(R.layout.main_fragment, container, false)
        recyclerForms = root.findViewById(R.id.form_recycler)
        tvEmpty = root.findViewById(R.id.tvEmpty)
        progressBar = root.findViewById(R.id.pbLoading)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerForms.layoutManager = LinearLayoutManager(context)
        recyclerForms.adapter = formsAdapter
        tvEmpty.visibility = View.GONE
        //val action = MainFragmentDirections.actionMainFragmentToFormFragment("Form","Y7Bo4cgXPXLuXeZlMhb7", null)

        viewModel.authenticationState.observe(viewLifecycleOwner, Observer { authenticationState ->
            when(authenticationState!!){
                AuthenticationState.UNAUTHENTICATED -> findNavController().navigate(R.id.login_fragment)
                AuthenticationState.INVALID_AUTHENTICATION -> Timber.e(getString(R.string.login_unsuccessful))
                AuthenticationState.AUTHENTICATED -> {
                    viewModel.forms.observe(viewLifecycleOwner, Observer { filledForms ->
                        formsAdapter.items = filledForms
                        progressBar.visibility = View.GONE
                        tvEmpty.visibility = when(filledForms.size){
                            0 -> View.VISIBLE
                            else -> View.GONE
                        }
                    })
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()
        val eventDisposable = RxBus.observeEventOnUi<EventFormSaved>()
            .onBackpressureLatest()
            .subscribe({ eventFormSaved: EventFormSaved ->
                showSavedMessage(eventFormSaved.form)
            }, { error ->
                Timber.e(error)
            })
        compositeDisposable.add(eventDisposable)
    }

    override fun onStop() {
        super.onStop()
        if (compositeDisposable.isDisposed) compositeDisposable.dispose()

    }
    
    private fun openForm(form: FilledForm){
        val action = MainFragmentDirections.actionMainToForm(form.description, form.id, form.alias, true)
        findNavController().navigate(action)
    }

    private fun showSavedMessage(form: FilledForm){

        val snackBar = Snackbar.make(root,"", Snackbar.LENGTH_LONG)

        if (form.finished){
            snackBar.setText(getString(R.string.finished_and_saved ))
        } else {
            snackBar.setText(getString(R.string.unfinished_but_saved))
            snackBar.setAction(getString(R.string.fill)) {
                openForm(form)
            }
        }

        snackBar.show()
    }
}
