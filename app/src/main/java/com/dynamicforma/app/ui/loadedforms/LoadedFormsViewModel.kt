package com.dynamicforma.app.ui.loadedforms

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.dynamicforma.app.data.AppRepository

class LoadedFormsViewModel @ViewModelInject constructor(
    private val appRepository: AppRepository
) : ViewModel() {

    val forms = appRepository.loadCachedForms()
}
