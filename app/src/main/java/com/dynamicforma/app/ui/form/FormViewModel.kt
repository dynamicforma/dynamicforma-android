package com.dynamicforma.app.ui.form

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.dynamicforma.app.EventFormSaved
import com.dynamicforma.app.RxBus
import com.dynamicforma.app.data.AppRepository

class FormViewModel @ViewModelInject constructor(
    private val appRepository: AppRepository
) : ViewModel() {

    lateinit var formId: String
    var isModified = false
    var isFilled = false
    val fields by lazy { appRepository.loadFields(formId, isFilled) }
    val form by lazy { appRepository.loadFormById(formId, isFilled) }

    fun save(){
        val flag = formValidation()
        form.value?.finished = flag
        appRepository.saveForms(form.value!!, fields.value!!){ state, savedFilledForm ->
            if (state) RxBus.postEvent(EventFormSaved(savedFilledForm!!))
        }
    }

    private fun formValidation(): Boolean{
        var isFormReady = true
        fields.value?.forEach {
            if (!isFilled) it.active = true // si no es un fromulario "llenado" se le asigna true para que aparezca el msj de campo requerido al momento de editar

            if (!isFormReady) return@forEach
            if (it.required){
                isFormReady = it.isAnswered()
            }
        }
        return isFormReady
    }
}
