package com.dynamicforma.app.ui.form

import android.graphics.Point
import android.os.Bundle
import android.view.*
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.fragment.app.Fragment
import android.widget.ProgressBar
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.dynamicforma.app.EventFormModified
import com.dynamicforma.app.R
import com.dynamicforma.app.RxBus
import com.dynamicforma.app.databinding.FormBackdropBinding
import com.dynamicforma.app.observeEventOnUi
import com.dynamicforma.app.view.common.BackdropToggler
import com.dynamicforma.app.view.common.MarginItemDecoration
import com.dynamicforma.app.view.common.PreCachingLayoutManager
import com.dynamicforma.app.view.common.toast
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

@AndroidEntryPoint
class FormFragment : Fragment() {


    private val viewModel: FormViewModel by viewModels()
    private val layoutManager = PreCachingLayoutManager(context)
    private lateinit var fieldsRecycler: RecyclerView
    private val fieldAdapter = FieldListAdapter()
    private lateinit var progressBar: ProgressBar
    private lateinit var tvEmpty: View
    private lateinit var backdropBinding: FormBackdropBinding
    private lateinit var backdrop: BackdropToggler
    private lateinit var toolbar: Toolbar
    private val args: FormFragmentArgs by navArgs()
    private lateinit var appBarOutlineProvider: ViewOutlineProvider
    private lateinit var appBarLayout: AppBarLayout
    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            // Handle the back button event
            if (viewModel.isModified){
                MaterialAlertDialogBuilder(context)
                    .setMessage(getString(R.string.discard_message))
                    .setPositiveButton(getString(R.string.discard_label)){ _, _ ->
                        findNavController().popBackStack()
                    }
                    .setNegativeButton(getString(R.string.cancel_lable), null)
                    .create()
                    .show()
            }else {
                findNavController().popBackStack()
            }

        }
        callback.isEnabled = true
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val root = inflater.inflate(R.layout.form_fragment, container, false)
        fieldsRecycler = root.findViewById(R.id.fields_recycler)
        progressBar = root.findViewById(R.id.pbLoading)
        tvEmpty = root.findViewById(R.id.tvEmpty)
        layoutManager.setExtraLayoutSpace(getExtraSpace())
        backdropBinding = FormBackdropBinding.bind(root.findViewById(R.id.form_backdrop))
        toolbar = activity?.findViewById(R.id.toolbar)!!
        val menuIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_menu_24dp)
        toolbar.navigationIcon = menuIcon

        backdrop = BackdropToggler(
            requireActivity(),
            root?.findViewById(R.id.form_body)!!,
            root.findViewById(R.id.disable_form)!!,
            AccelerateDecelerateInterpolator(),
            menuIcon, // Menu open icon
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_close_24dp)// Menu close icon
        )
        appBarLayout = activity?.findViewById(R.id.app_bar_layout)!!
        appBarOutlineProvider = appBarLayout.outlineProvider!!
        appBarLayout.outlineProvider = null

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fieldsRecycler.addItemDecoration(
            MarginItemDecoration(
            resources.getDimensionPixelSize(R.dimen.standard_padding_16))
        )
        fieldsRecycler.layoutManager = layoutManager
        fieldsRecycler.adapter = fieldAdapter
        tvEmpty.visibility = View.GONE

        viewModel.isModified = false
        viewModel.formId = args.formId
        viewModel.isFilled = args.isFilled

        viewModel.fields.observe(viewLifecycleOwner, Observer {
            fieldAdapter.items = it
            progressBar.visibility = View.GONE
            layoutManager.setExtraLayoutSpace(0)
        })

        viewModel.form.observe(viewLifecycleOwner, Observer { form ->
            form?.let {
                backdropBinding.form = it
                activity?.title = when(val name = args.formName){
                    null -> form.description
                    else -> name
                }
            }
        })

    }

    override fun onStart() {
        super.onStart()
        val disposable = RxBus.observeEventOnUi<EventFormModified>()
            .onBackpressureLatest()
            .doOnError { Timber.e(it) }
            .subscribe({ eventFormModified ->
                viewModel.isModified = eventFormModified.isFormModified
            }) { Timber.e(it) }

        compositeDisposable.add(disposable)
    }

    override fun onStop() {
        super.onStop()
        if (compositeDisposable.isDisposed) compositeDisposable.dispose()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.form_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when(item.itemId){
        android.R.id.home -> {
            backdrop.toggle(toolbar)
            true
        }
        R.id.action_send -> {
            if (viewModel.isModified){
                viewModel.save()
                findNavController().popBackStack(R.id.main_fragment, false)
            } else {
                activity?.toast(getString(R.string.form_unmodified))
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun getExtraSpace(): Int{
        val display = activity?.windowManager?.defaultDisplay
        val size = Point()
        display?.getSize(size)
        return size.y
    }

    override fun onDestroyView() {
        super.onDestroyView()
        appBarLayout.outlineProvider = appBarOutlineProvider
    }
}
