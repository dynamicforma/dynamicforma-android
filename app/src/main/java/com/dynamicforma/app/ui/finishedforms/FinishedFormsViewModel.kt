package com.dynamicforma.app.ui.finishedforms

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import com.dynamicforma.app.data.AppRepository
import com.dynamicforma.app.data.entities.FilledForm

class FinishedFormsViewModel @ViewModelInject constructor(
    private val appRepository: AppRepository
) : ViewModel() {
    val forms: LiveData<List<FilledForm>> = LiveDataReactiveStreams.fromPublisher(appRepository.loadFilledForms(true))
}
