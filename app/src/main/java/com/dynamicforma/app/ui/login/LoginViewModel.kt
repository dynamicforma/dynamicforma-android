package com.dynamicforma.app.ui.login

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.dynamicforma.app.data.AppRepository
import com.dynamicforma.app.utils.AuthenticationState
import com.dynamicforma.app.utils.FirebaseUserLiveData

class LoginViewModel @ViewModelInject constructor(
    private val appRepository: AppRepository,
    private val firebaseUserLiveData: FirebaseUserLiveData
) : ViewModel() {

    val authenticationState = firebaseUserLiveData.map { user ->
        if (user != null){
            AuthenticationState.AUTHENTICATED
        }else{
            AuthenticationState.UNAUTHENTICATED
        }
    }

    fun saveUser(){
        appRepository.saveUser()
    }
}