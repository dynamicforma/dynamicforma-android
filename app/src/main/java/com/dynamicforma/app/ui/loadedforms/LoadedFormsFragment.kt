package com.dynamicforma.app.ui.loadedforms


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dynamicforma.app.R
import com.dynamicforma.app.data.entities.Form
import com.dynamicforma.app.view.common.ItemListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoadedFormsFragment : Fragment() {

    private val viewModel: LoadedFormsViewModel by viewModels()
    private lateinit var recyclerForms: RecyclerView
    private val formsAdapter = FormListAdapter(ItemListener { openForm(it) }).apply { setHasStableIds(true) }
    private lateinit var tvEmpty: View
    private lateinit var progressBar: ProgressBar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val root = inflater.inflate(R.layout.loaded_forms_fragment, container, false)
        recyclerForms = root.findViewById(R.id.form_recycler)
        tvEmpty = root.findViewById(R.id.tvEmpty)
        progressBar = root.findViewById(R.id.pbLoading)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerForms.layoutManager = LinearLayoutManager(context)
        recyclerForms.adapter = formsAdapter
        tvEmpty.visibility = View.GONE

        viewModel.forms.observe(viewLifecycleOwner, Observer { forms ->
            forms?.let {
                formsAdapter.items = it
                progressBar.visibility = View.GONE
                tvEmpty.visibility = when(it.size){
                    0 -> View.VISIBLE
                    else -> View.GONE
                }
            }
        })
    }

    private fun openForm(form: Form){
        val action = LoadedFormsFragmentDirections.actionLoadedFormsToForm(form.description, form.id)
        findNavController().navigate(action)
    }

}
