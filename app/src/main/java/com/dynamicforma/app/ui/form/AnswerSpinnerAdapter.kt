package com.dynamicforma.app.ui.form

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.dynamicforma.app.data.entities.Answer
import kotlin.properties.Delegates

class AnswerSpinnerAdapter(
    private val contextAdapter: Context

) : ArrayAdapter<Answer>(contextAdapter, 0){

    var answers by Delegates.observable(listOf<Answer>()){ _, oldValue, newValue ->
        if (oldValue != newValue){
            notifyDataSetChanged()
        }
    }
    override fun getCount() = answers.size

    override fun getItem(position: Int) = answers[position]

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup) = getView(position, convertView, parent)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = when(convertView){
             null -> {
                val inflater = LayoutInflater.from(contextAdapter)
                inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
            }
            else -> convertView
        }

        val textView = view.findViewById<TextView>(android.R.id.text1)
        textView.text = answers[position].description
        return view
    }

    fun getSelectedAnswerPosition() = try {
        answers.indexOf(answers.first { answer -> answer.chosen == true  })
    }catch (e: NoSuchElementException){
       -1
    }
}