package com.dynamicforma.app.ui.main


import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.dynamicforma.app.data.AppRepository
import com.dynamicforma.app.data.entities.FilledForm
import com.dynamicforma.app.utils.AuthenticationState
import com.dynamicforma.app.utils.FirebaseUserLiveData

class MainViewModel @ViewModelInject constructor(
    private val appRepository: AppRepository,
    private val firebaseUserLiveData: FirebaseUserLiveData
) : ViewModel() {

    val authenticationState = firebaseUserLiveData.map {user ->
        if (user != null) {
            AuthenticationState.AUTHENTICATED
        } else {
            AuthenticationState.UNAUTHENTICATED
        }

    }
    val forms: LiveData<List<FilledForm>> = LiveDataReactiveStreams.fromPublisher(appRepository.loadFilledForms(false))

}
