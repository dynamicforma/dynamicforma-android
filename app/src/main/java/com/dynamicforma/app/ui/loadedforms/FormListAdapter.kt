package com.dynamicforma.app.ui.loadedforms

import com.dynamicforma.app.BR
import com.dynamicforma.app.R
import com.dynamicforma.app.data.entities.Form
import com.dynamicforma.app.databinding.FormItemBinding
import com.dynamicforma.app.view.common.ItemListener
import com.dynamicforma.app.view.common.AnyListAdapter

class FormListAdapter(
    private  val clickListener: ItemListener<Form>
) : AnyListAdapter<Form,FormItemBinding>(R.layout.form_item, BR.form, clickListener)