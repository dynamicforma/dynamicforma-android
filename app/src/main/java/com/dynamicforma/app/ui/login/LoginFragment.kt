package com.dynamicforma.app.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.dynamicforma.app.R
import com.dynamicforma.app.utils.AuthenticationState
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : Fragment() {
    @Inject lateinit var auth: FirebaseAuth
    private val viewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            activity?.finish()
        }
        callback.isEnabled = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.authenticationState.observe(viewLifecycleOwner, Observer { authenticationState ->
            when(authenticationState!!){
                AuthenticationState.UNAUTHENTICATED -> singInAnonymously()
                AuthenticationState.INVALID_AUTHENTICATION -> Timber.e(getString(R.string.login_unsuccessful))
                AuthenticationState.AUTHENTICATED -> {
                    viewModel.saveUser()
                    findNavController().popBackStack()
                }
            }
        })
    }

    private fun singInAnonymously() {
        auth.signInAnonymously().addOnCompleteListener(requireActivity()) { task ->
            if (task.isSuccessful) {
                Timber.i("signInAnonymously:success")
            } else {
                Timber.e(task.exception, "signInAnonymously:failure")
            }

        }
    }
}