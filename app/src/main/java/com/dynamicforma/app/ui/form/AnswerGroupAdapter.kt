package com.dynamicforma.app.ui.form

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dynamicforma.app.data.FieldTypeEnum
import com.dynamicforma.app.data.entities.Answer
import com.dynamicforma.app.view.answer.CheckBoxDelegate
import com.dynamicforma.app.view.answer.DynamicAnswer
import com.dynamicforma.app.view.answer.RadioButtonDelegate
import com.dynamicforma.app.view.common.AutoUpdatableAdapter
import com.dynamicforma.app.view.field.delegates.RadioButtonGroupDelegate

import kotlin.properties.Delegates

class AnswerGroupAdapter(
    private val type: Int = 0
) : RecyclerView.Adapter<AnswerGroupAdapter.AnswerViewHolder>(), AutoUpdatableAdapter{

    companion object{
        val viewPool = RecyclerView.RecycledViewPool().apply { setMaxRecycledViews( 2,20) }
    }

    lateinit var answerChosenListener: RadioButtonGroupDelegate.AnswerChosenListener
    var items: List<Answer> by Delegates.observable(emptyList()){ _, oldValue, newValue ->
        autoNotify(oldValue, newValue) { old, new -> old.id == new.id}
    }

    init {
        setHasStableIds(true)
    }

    override fun getItemViewType(position: Int) = type

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : AnswerViewHolder{
        val answer = createAnswer(viewType, parent.context)
        return AnswerViewHolder(answer)
    }

    override fun onBindViewHolder(holder: AnswerViewHolder, position: Int) {
        holder.answer.bindTo(items[position])
    }

    private fun createAnswer(type: Int, context: Context): DynamicAnswer = when(type){
        FieldTypeEnum.RadioGroup.ordinal -> RadioButtonDelegate(context, answerChosenListener)
        FieldTypeEnum.CheckGroup.ordinal -> CheckBoxDelegate(context)
        else -> CheckBoxDelegate(context)
    }

    override fun getItemId(position: Int) = items[position].id.hashCode().toLong()

    inner class AnswerViewHolder(val answer: DynamicAnswer) : RecyclerView.ViewHolder(answer.getRoot())
}