package com.dynamicforma.app.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@InstallIn(ApplicationComponent::class)
@Module
class AppModule{

    @Provides
    fun providesFirestoreDb() = Firebase.firestore

    @Provides
    fun providesFirebaseAuth() = FirebaseAuth.getInstance()

}