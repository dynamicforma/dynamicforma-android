package com.dynamicforma.app

import android.app.Application
import android.os.StrictMode
import dagger.hilt.android.HiltAndroidApp
import io.grpc.android.BuildConfig
import timber.log.Timber


@HiltAndroidApp
class DynamicFormaApp : Application(){

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG){
            StrictMode.enableDefaults()
            Timber.plant(Timber.DebugTree())
        }
    }

}