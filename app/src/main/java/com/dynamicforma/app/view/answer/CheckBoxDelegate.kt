package com.dynamicforma.app.view.answer

import android.content.Context
import android.view.LayoutInflater
import com.dynamicforma.app.data.entities.Answer
import com.dynamicforma.app.databinding.CheckBoxBinding

class CheckBoxDelegate(
    context: Context
) : DynamicAnswer {

    private val binding = CheckBoxBinding.inflate(LayoutInflater.from(context))

    override fun bindTo(answer: Answer) {
        binding.answer = answer
        binding.executePendingBindings()
    }

    override fun getRoot() = binding.root
}