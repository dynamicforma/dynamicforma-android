package com.dynamicforma.app.view.answer

import android.content.Context
import android.view.LayoutInflater
import com.dynamicforma.app.data.entities.Answer
import com.dynamicforma.app.databinding.RadioButtonBinding

class RadioButtonDelegate(
    context: Context,
    private val answerChosenListener: Listener
) : DynamicAnswer {

    private val binding = RadioButtonBinding.inflate(LayoutInflater.from(context))

    interface Listener{
        fun onRadioClicked(answer: Answer)
    }

    init {
        binding.radioButton.setOnClickListener { answerChosenListener.onRadioClicked(binding.answer!!) }
    }

    override fun bindTo(answer: Answer) {
        binding.answer = answer
    }

    override fun getRoot() = binding.root
}