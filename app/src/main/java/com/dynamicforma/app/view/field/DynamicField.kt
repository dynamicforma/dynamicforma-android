package com.dynamicforma.app.view.field

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.marginBottom
import com.dynamicforma.app.R
import com.dynamicforma.app.data.FieldTypeEnum
import com.dynamicforma.app.data.entities.FilledField
import com.dynamicforma.app.view.field.delegates.*

class DynamicField @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, type: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr){


    private val fieldDelegate = createField(type)


    init {
        layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        val dpAsPixels = resources.getDimensionPixelSize(R.dimen.standard_padding_16)
        setPadding(dpAsPixels,dpAsPixels,dpAsPixels,dpAsPixels)
//        background = resources.getDrawable(R.drawable.round_bg, null)
    }

    fun bindTo(f: FilledField){
        fieldDelegate.field = f
    }

    private fun createField(type: Int) = when(type){
        FieldTypeEnum.SimpleText.ordinal -> SimpleTextDelegate(
            R.layout.simple_text,
            this
        )
        FieldTypeEnum.SimpleSelect.ordinal -> SimpleSelectDelegate(
            R.layout.simple_select,
            this
        )
        FieldTypeEnum.RadioGroup.ordinal -> RadioButtonGroupDelegate(
            R.layout.check_radio_group,
            this
        )
        FieldTypeEnum.CheckGroup.ordinal -> CheckBoxGroupDelegate(
            R.layout.check_radio_group,
            this
        )
        FieldTypeEnum.DataPicker.ordinal -> DatePickerDelegate(
            R.layout.date_picker,
            this
        )
        else -> SimpleTextDelegate(
            R.layout.simple_text,
            this
        )
    }

}