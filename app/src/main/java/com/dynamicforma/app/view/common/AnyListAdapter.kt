package com.dynamicforma.app.view.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.dynamicforma.app.data.BasicProperties
import kotlin.properties.Delegates

abstract class AnyListAdapter<T : BasicProperties, B: ViewDataBinding>(
    @LayoutRes private val layoutId: Int,
    private val variableId: Int,
    private val clickListener: ItemListener<T>
) : RecyclerView.Adapter<AnyListAdapter.ViewHolder<T>>(), AutoUpdatableAdapter {

    var items: List<T> by Delegates.observable(emptyList()) { _, oldValue, newValue ->
        autoNotify(oldValue, newValue) { old, new -> old.id == new.id}
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<T>{
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate(layoutInflater, layoutId, parent, false) as B
        return ViewHolder(binding)
    }


    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder<T>, position: Int) {
        holder.bind(clickListener, items[position], variableId)
    }

    override fun getItemId(position: Int) = items[position].id.hashCode().toLong()

    class ViewHolder<T> constructor(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(clickListener: ItemListener<T>, form: T, variableId: Int) {
            binding.setVariable(variableId, form)
            binding.root.setOnClickListener { clickListener.onClick(form) }
            binding.executePendingBindings()
        }
    }
}

class ItemListener<T>(val clickListener: (t: T) -> Unit) {
    fun onClick(t: T) = clickListener(t)
}