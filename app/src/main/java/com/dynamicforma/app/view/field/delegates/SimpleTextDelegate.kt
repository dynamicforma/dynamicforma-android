package com.dynamicforma.app.view.field.delegates

import android.view.LayoutInflater
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import com.dynamicforma.app.data.decorators.FieldViewData
import com.dynamicforma.app.data.entities.FilledField
import com.dynamicforma.app.databinding.SimpleTextBinding
import com.dynamicforma.app.view.field.DynamicField
import com.dynamicforma.app.view.field.IDynamicField
import kotlin.properties.Delegates

class SimpleTextDelegate(@LayoutRes layoutRes: Int, field: DynamicField) :
    IDynamicField {

    override var field by Delegates.observable(FilledField()) { _, oldValue, newValue ->
        if (oldValue != newValue) bindData()
    }

    private val binding: SimpleTextBinding =
        DataBindingUtil.inflate(LayoutInflater.from(field.context), layoutRes, field, true)

    private fun bindData(){
        binding.field = FieldViewData(field)
        binding.executePendingBindings()
    }
}