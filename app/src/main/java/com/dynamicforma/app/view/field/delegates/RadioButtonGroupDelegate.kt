package com.dynamicforma.app.view.field.delegates

import android.view.LayoutInflater
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.dynamicforma.app.R
import com.dynamicforma.app.data.FieldTypeEnum
import com.dynamicforma.app.data.decorators.FieldViewData
import com.dynamicforma.app.data.entities.Answer
import com.dynamicforma.app.data.entities.FilledField
import com.dynamicforma.app.databinding.CheckRadioGroupBinding
import com.dynamicforma.app.ui.form.AnswerGroupAdapter
import com.dynamicforma.app.view.answer.RadioButtonDelegate
import com.dynamicforma.app.view.field.DynamicField
import com.dynamicforma.app.view.field.IDynamicField
import kotlin.properties.Delegates

class RadioButtonGroupDelegate(@LayoutRes layoutRes: Int, field: DynamicField) :
    IDynamicField {


    override var field by Delegates.observable(FilledField()) { _, oldValue, newValue ->
        if (oldValue != newValue) bindData()
    }

    private val binding: CheckRadioGroupBinding =
        DataBindingUtil.inflate(LayoutInflater.from(field.context), layoutRes, field, true)

    private val adapter = AnswerGroupAdapter(type = FieldTypeEnum.RadioGroup.ordinal).apply {
        answerChosenListener = AnswerChosenListener()
    }

    init {
        val context = binding.root.context
        val columns  = context.resources.getInteger(R.integer.grid_view_colums)
        binding.answerGroupRecycler.setRecycledViewPool(AnswerGroupAdapter.viewPool)
        binding.answerGroupRecycler.setHasFixedSize(true)
        binding.answerGroupRecycler.layoutManager = GridLayoutManager(context, columns)
    }

    private fun bindData(){
        binding.field = FieldViewData(field)
        adapter.items = field.chosenAnswers ?: listOf()
        binding.answerGroupRecycler.adapter = adapter
        binding.executePendingBindings()
    }

    inner class AnswerChosenListener : RadioButtonDelegate.Listener {
        override fun onRadioClicked(answer: Answer) {
            field.chosenAnswers?.forEach {
                if (answer.id != it.id) it.chosen = !answer.chosen!!
            }
        }
    }
}