package com.dynamicforma.app.view.answer

import android.view.View
import com.dynamicforma.app.data.entities.Answer

interface DynamicAnswer {
    fun bindTo(answer: Answer)
    fun getRoot(): View
}