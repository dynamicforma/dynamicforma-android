package com.dynamicforma.app.view.field.delegates

import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import com.dynamicforma.app.EventFormModified
import com.dynamicforma.app.R
import com.dynamicforma.app.RxBus
import com.dynamicforma.app.data.decorators.FieldViewData
import com.dynamicforma.app.data.entities.FilledField
import com.dynamicforma.app.databinding.SimpleSelectBinding
import com.dynamicforma.app.ui.form.AnswerSpinnerAdapter
import com.dynamicforma.app.view.common.DefaultSpinnerAdapter
import com.dynamicforma.app.view.field.DynamicField
import com.dynamicforma.app.view.field.IDynamicField
import timber.log.Timber
import kotlin.properties.Delegates

class SimpleSelectDelegate(@LayoutRes layoutRes: Int, dynamicField: DynamicField) :
    IDynamicField {

    private lateinit var fieldViewData: FieldViewData
    override var field by Delegates.observable(FilledField()) { _, oldValue, newValue ->
        if (oldValue != newValue) bindData()
    }
    private val binding: SimpleSelectBinding =
        DataBindingUtil.inflate(LayoutInflater.from(dynamicField.context), layoutRes, dynamicField, true)

    private val answerAdapter = AnswerSpinnerAdapter(binding.root.context)
    private val defaultSelectionAdapter =
        DefaultSpinnerAdapter(answerAdapter, R.layout.default_spinner_view, adapterContext = binding.root.context)

    init {
        binding.spinner.adapter = defaultSelectionAdapter
        binding.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected( parent: AdapterView<*>?, view: View?, position: Int, id: Long ) {
                if ( position == 0 ) return

                if (position != fieldViewData.selectedAnswerPosition){
                    fieldViewData.selectedAnswerPosition = position
                    RxBus.postEvent(EventFormModified(true))
                    setNotChosenAnswer(position)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun bindData(){
        fieldViewData = FieldViewData(field)
        answerAdapter.answers = field.chosenAnswers ?: listOf()

        if (answerAdapter.getSelectedAnswerPosition() != -1) {
            fieldViewData.selectedAnswerPosition = answerAdapter.getSelectedAnswerPosition() + 1
        }

        binding.field = fieldViewData
        binding.executePendingBindings()
    }

    private fun setNotChosenAnswer(selectedPosition: Int){
        field.chosenAnswers?.forEach {
            val index = field.chosenAnswers?.indexOf(it)
            it.chosen = index == selectedPosition -1
        }
    }
}