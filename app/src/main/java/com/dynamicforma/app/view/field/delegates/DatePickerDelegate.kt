package com.dynamicforma.app.view.field.delegates

import android.app.DatePickerDialog
import android.os.SystemClock
import android.view.LayoutInflater
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import com.dynamicforma.app.R
import com.dynamicforma.app.data.decorators.FieldViewData
import com.dynamicforma.app.data.entities.FilledField
import com.dynamicforma.app.databinding.DatePickerBinding
import com.dynamicforma.app.view.field.DynamicField
import com.dynamicforma.app.view.field.IDynamicField
import timber.log.Timber
import java.lang.NullPointerException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates

class DatePickerDelegate(@LayoutRes layoutRes: Int, field: DynamicField) :
    IDynamicField {

    override var field by Delegates.observable(FilledField()) { _, oldValue, newValue ->
        if (oldValue != newValue) bindData()
    }

    private val binding: DatePickerBinding =
        DataBindingUtil.inflate(LayoutInflater.from(field.context), layoutRes, field, true)

    init {
        var lastClickTime: Long = 0
        binding.datePicker.setOnClickListener {
            if (SystemClock.elapsedRealtime() - lastClickTime < 1000) return@setOnClickListener
            lastClickTime = SystemClock.elapsedRealtime()
            selectDate()
        }
    }

    private fun bindData(){
        binding.field = FieldViewData(field)
        binding.executePendingBindings()
    }


    private fun selectDate(){
        val c = Calendar.getInstance()
        val y = c.get(Calendar.YEAR)
        val m = c.get(Calendar.MONTH)
        val d = c.get(Calendar.DAY_OF_MONTH)
        val pickerDialog = DatePickerDialog(binding.root.context, R.style.Widget_App_DatePicker,
            DatePickerDialog.OnDateSetListener { _, year, month, day ->
                try {
                    val dateFormat = SimpleDateFormat(binding.root.context.getString(R.string.date_format), Locale.getDefault())
                    var varMonth = month
                    varMonth += 1
                    val date = dateFormat.parse("$day/$varMonth/$year")!!
                    binding.datePicker.setText(dateFormat.format(date))
                }catch (e: NullPointerException){
                    Timber.e(e)
                }
            },y, m, d)
        pickerDialog.show()
    }

}