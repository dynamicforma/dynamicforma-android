package com.dynamicforma.app.view.common

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.util.DisplayMetrics
import android.view.View
import android.view.animation.Interpolator
import androidx.appcompat.widget.Toolbar
import com.dynamicforma.app.R

class BackdropToggler constructor(
    private val context: Context, private val sheet: View, private val disableSheet: View, private val interpolator: Interpolator? = null,
    private val openIcon: Drawable? = null, private val closeIcon: Drawable? = null) {

    private val animatorSet = AnimatorSet()
    private val height: Int
    private var backdropShown = false

    init {
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        height = displayMetrics.heightPixels


    }

    fun toggle(toolbar: Toolbar) {
        backdropShown = !backdropShown

        // Cancel the existing animations
        animatorSet.removeAllListeners()
        animatorSet.end()
        animatorSet.cancel()

        updateIcon(toolbar)

        val translateY = height - context.resources.getDimensionPixelSize(R.dimen.form_reveal_height)

        val value = ( if (backdropShown) {
            disableSheet.visibility = View.VISIBLE
            sheet.clearFocus()
            translateY
        } else {
            disableSheet.visibility = View.GONE;
            0
        }).toFloat()
        val animator = ObjectAnimator.ofFloat(sheet, "translationY", value)
        animator.duration = 500
        if (interpolator != null) {
            animator.interpolator = interpolator
        }
        animatorSet.play(animator)
        animator.start()
    }

    private fun updateIcon(toolbar: Toolbar) {
        if (openIcon != null && closeIcon != null) {
            if (backdropShown) {
                toolbar.navigationIcon = closeIcon
            } else {
                toolbar.navigationIcon = openIcon
            }
        }
    }
}
