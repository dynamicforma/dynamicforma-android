package com.dynamicforma.app.view.field

import com.dynamicforma.app.data.entities.FilledField

interface IDynamicField{
    var field: FilledField
}