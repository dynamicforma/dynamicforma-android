package com.dynamicforma.app.view.common

import android.content.Context
import android.database.DataSetObserver
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListAdapter
import android.widget.SpinnerAdapter

class DefaultSpinnerAdapter(
    val adapter: SpinnerAdapter,
    val nothingSelectedLayout: Int,
    val nothingSelectedDropdownLayout: Int = -1,
    val adapterContext: Context
) : SpinnerAdapter, ListAdapter{

    companion object{
        val EXTRA: Int = 1
    }

    val layoutInflater = LayoutInflater.from(adapterContext)

    // Android BUG! http://code.google.com/p/android/issues/detail?id=17128 -
    // Spinner does not support multiple view types
    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup) = when{
        position == 0 -> when{
            nothingSelectedDropdownLayout == -1 -> View(adapterContext)
            else -> getNothingSelectedDropdownView(parent)
        }
        else -> adapter.getDropDownView(position - EXTRA, null, parent) // Could re-use the convertView if possible, use setTag...
    }

    protected fun getNothingSelectedDropdownView(parent: ViewGroup) =layoutInflater.inflate(nothingSelectedDropdownLayout, parent, false)

    override fun registerDataSetObserver(observer: DataSetObserver) {
        adapter.registerDataSetObserver(observer)
    }

    override fun unregisterDataSetObserver(observer: DataSetObserver?) {
        adapter.unregisterDataSetObserver(observer)
    }

    override fun getCount() = when{
        adapter.count == 0 -> adapter.count
        else -> adapter.count + EXTRA
    }

    override fun getItem(position: Int) = when{
        position == 0 -> null
        else -> adapter.getItem(position - EXTRA)
    }

    override fun getItemId(position: Int) = when{
        position >= EXTRA -> adapter.getItemId(position - EXTRA)
        else -> (position - EXTRA).toLong()
    }

    override fun hasStableIds() = adapter.hasStableIds()

    //View para mostrar mensaje de "seleccione un elemento"
    override fun getView(position: Int, convertView: View?, parent: ViewGroup) = when{
        position == 0 -> getNothingSelectedView(parent)
        else -> adapter.getView(position - EXTRA, null, parent)
    }

    protected fun getNothingSelectedView(parent: ViewGroup) = layoutInflater.inflate(nothingSelectedLayout, parent, false)

    override fun getItemViewType(position: Int) = 0

    override fun getViewTypeCount() = 1

    override fun isEmpty() = adapter.isEmpty

    override fun areAllItemsEnabled() = false

    override fun isEnabled(position: Int) = position != 0
}