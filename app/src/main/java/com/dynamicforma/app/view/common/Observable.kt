package com.dynamicforma.app.view.common

import androidx.databinding.BaseObservable
import com.dynamicforma.app.EventFormModified
import com.dynamicforma.app.RxBus
import kotlin.properties.ObservableProperty
import kotlin.reflect.KProperty

class BindableProperty<T>(
    initial: T,
    private val observable: BaseObservable,
    private val id: Int
) : ObservableProperty<T>(initial) {

    override fun beforeChange(
        property: KProperty<*>,
        oldValue: T,
        newValue: T
    ): Boolean = oldValue != newValue

    override fun afterChange(
        property: KProperty<*>,
        oldValue: T,
        newValue: T
    ) {
        if (oldValue != null){
            RxBus.postEvent(EventFormModified(true))
        }
        observable.notifyPropertyChanged(id)
    }
}