package com.dynamicforma.app.view.common

import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import com.dynamicforma.app.R
import com.dynamicforma.app.data.decorators.FieldViewData
import com.google.android.material.textfield.TextInputLayout

object BindingAdapters{
    @BindingAdapter("imageResource")
    @JvmStatic fun setNumberOfSets(imageView: ImageView, @DrawableRes id: Int) {
        imageView.setImageResource(id)
    }

    @BindingAdapter("validateTextField")
    @JvmStatic fun setValidation(textInputLayout: TextInputLayout, field: FieldViewData){
        val message = textInputLayout.context.getString(R.string.required_field)
        when {
            field.errorMessageVisible -> {
                textInputLayout.isHelperTextEnabled = false
                textInputLayout.isErrorEnabled = true
                textInputLayout.error = message
            }
            field.required -> {
                textInputLayout.isErrorEnabled = false
                textInputLayout.isHelperTextEnabled = true
                textInputLayout.helperText = message
            }
            else -> {
                textInputLayout.isErrorEnabled = false
                textInputLayout.isHelperTextEnabled = false
            }
        }

    }
}