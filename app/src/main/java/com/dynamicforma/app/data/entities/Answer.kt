package com.dynamicforma.app.data.entities

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.dynamicforma.app.data.BasicProperties
import com.dynamicforma.app.view.common.BindableProperty
import com.google.firebase.firestore.Exclude
import java.util.*


data class Answer(
    @get:Exclude override val id: String = UUID.randomUUID().toString(),
    override var description: String = "",
    private val initialChosen: Boolean? = null,
    @get:Exclude override var active: Boolean = false
) : BaseObservable(), BasicProperties {
    @get:Bindable
    var chosen by BindableProperty(initialChosen,this, BR.chosen)
}