package com.dynamicforma.app.data.entities

import com.dynamicforma.app.data.BasicProperties
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentId

data class Form(
    @DocumentId override val id: String = "",
    var creatorName: String = "",
    override var description: String = "",
    override var active: Boolean = false,
    var details: String = "",
    var ownerId: String = "",
    var created: Timestamp? = null
) : BasicProperties