package com.dynamicforma.app.data.entities

data class User(
    var name: String,
    var email: String,
    var anonymous: Boolean
)