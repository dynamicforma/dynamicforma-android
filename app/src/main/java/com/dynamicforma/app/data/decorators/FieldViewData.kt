package com.dynamicforma.app.data.decorators

import com.dynamicforma.app.EventFormModified
import com.dynamicforma.app.RxBus
import com.dynamicforma.app.data.entities.FilledField

class FieldViewData(
    private val field: FilledField
) {

    //field.active es inicializado en false cuando es un formulario 'nuevo'
    var errorMessageVisible = !field.isAnswered() && field.required && field.active
    var selectedAnswerPosition = 0
    
    var textAnswer: String
        get () = this.field.textAnswer ?: ""
        set(value) {
            RxBus.postEvent(EventFormModified(true))
            this.field.textAnswer = value
        }

    val required: Boolean
        get() = this.field.required

    val description: String
        get() = this.field.description

}