package com.dynamicforma.app.data.entities

import com.dynamicforma.app.data.FieldCommon
import com.dynamicforma.app.data.FieldTypeEnum
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude

data class FilledField(
    @DocumentId override val id: String = "",
    var chosenAnswers: List<Answer>? = null,
    var textAnswer: String? = null,
    override var fieldType: Int = -1,
    override var order: Int = -1,
    override var required: Boolean = false,
    override var description: String = "",
    override var active: Boolean = false
) : FieldCommon{

    private fun multipleValidation(): Boolean {
        var flag = false
        chosenAnswers?.forEach { answer: Answer ->
            answer.chosen?.let {
                if (it) flag = true
            }
        }
        return flag
    }

    private fun singleValidation(): Boolean {
        return !textAnswer.isNullOrEmpty()
    }

    @Exclude
    fun isAnswered() = when(fieldType) {
        FieldTypeEnum.DataPicker.ordinal -> singleValidation()
        FieldTypeEnum.SimpleText.ordinal -> singleValidation()
        FieldTypeEnum.SimpleSelect.ordinal -> multipleValidation()
        FieldTypeEnum.RadioGroup.ordinal -> multipleValidation()
        FieldTypeEnum.CheckGroup.ordinal -> multipleValidation()

        else -> false
    }
}