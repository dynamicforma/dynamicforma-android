package com.dynamicforma.app.data.entities

import com.dynamicforma.app.data.BasicProperties
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.ServerTimestamp

data class FilledForm(
    @DocumentId override val id: String = "",
    var creatorName: String = "",
    var templateFormId: String = "",
    var alias: String? = null,
    var user: String = "",
    override var description: String = "",
    var finished: Boolean = false,
    @ServerTimestamp var created: Timestamp? = null,
    @ServerTimestamp var updated: Timestamp? = null,
    override var active: Boolean = false
) : BasicProperties