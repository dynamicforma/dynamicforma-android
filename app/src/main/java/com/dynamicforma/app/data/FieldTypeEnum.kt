package com.dynamicforma.app.data

enum class FieldTypeEnum{
    SimpleText, SimpleSelect, CheckGroup, RadioGroup, SearchSelect, DataPicker
}