package com.dynamicforma.app.data

interface BasicProperties{
    val id: String
    var description: String
    var active: Boolean
}