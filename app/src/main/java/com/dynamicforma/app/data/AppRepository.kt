package com.dynamicforma.app.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dynamicforma.app.data.entities.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppRepository @Inject constructor(){
    companion object{
        const val FORMS_COLL = "Forms"
        const val FIELDS_COLL = "Fields"
        const val FILLED_FORMS_COLL = "FilledForms"
        const val FILLED_FIELDS_COLL = "FilledFields"
        const val USERS_COLL = "Users"
        const val USER_KEY = "user"
        const val FORM_FINISHED_KEY = "finished"
        const val ORDER_KEY = "order"
        const val UPDATED_KEY = "updated"
    }

    @Inject lateinit var db: FirebaseFirestore
    @Inject lateinit var auth: FirebaseAuth
    private val user by lazy { auth.currentUser }

    fun loadFilledForms(loadFinished: Boolean): Flowable<List<FilledForm>> {

        return Flowable.create<List<FilledForm>>({ emitter ->

            val listenerRegistration = db.collection(FILLED_FORMS_COLL)
                .whereEqualTo(FORM_FINISHED_KEY, loadFinished)
                .whereEqualTo(USER_KEY, user?.uid)
                .orderBy(UPDATED_KEY,Query.Direction.DESCENDING)
                .addSnapshotListener { querySnapshot, firebaseFirestoreException ->
                    firebaseFirestoreException?.let {
                        Timber.e(firebaseFirestoreException)
                        emitter.onError(firebaseFirestoreException)
                        return@addSnapshotListener
                    }
                    querySnapshot?.let {
                        val list = it.toObjects<FilledForm>()
                        emitter.onNext(list)
                    }
                }

            emitter.setCancellable { listenerRegistration.remove() }
        }, BackpressureStrategy.LATEST)
    }

    fun loadFormById (formId: String, isFilled: Boolean) : LiveData<FilledForm> {
        val form = MutableLiveData<FilledForm>()
        val collection = if (isFilled) FILLED_FORMS_COLL else FORMS_COLL
        db.collection(collection)
            .document(formId)
            .get()
            .addOnSuccessListener {
                if (isFilled){
                    it.toObject<FilledForm>()?.let { f ->
                        form.value = f
                    }
                }else{
                    it.toObject<Form>()?.let { f ->
                        form.value = formToFilledForm(f)
                    }
                }

            }.addOnFailureListener {
                Timber.e(it)
            }
        return form
    }

    fun loadCachedForms(): LiveData<List<Form>>{
        val list = MutableLiveData<List<Form>>()
        db.collection(FORMS_COLL)
            .get(Source.CACHE)
            .addOnSuccessListener {
                list.value = it.toObjects()
            }.addOnFailureListener { Timber.e(it) }
        return list
    }

    fun loadFields(formId: String, isFilled: Boolean) : LiveData<List<FilledField>>{
        val fields = MutableLiveData<List<FilledField>>()
        val collection = if (isFilled) FILLED_FORMS_COLL else FORMS_COLL
        val subCollection = if (isFilled) FILLED_FIELDS_COLL else FIELDS_COLL
        db.collection(collection)
            .document(formId)
            .collection(subCollection)
            .orderBy(ORDER_KEY,Query.Direction.ASCENDING)
            .get(Source.SERVER)
            .addOnSuccessListener {
                fields.value = if (isFilled){
                   it.toObjects()
                }else{
                    it.toObjects<Field>().map(this::fieldToFilledField)
                }
            }.addOnFailureListener {
                Timber.e(it)
            }
        return fields
    }

    fun saveUser(){
        user?.let {
            val u = User(it.displayName ?: "", it.email ?: "",it.isAnonymous)
            db.collection(USERS_COLL).document(it.uid).set(u)
        }

    }

    fun saveForms(filledForm: FilledForm, filledFields: List<FilledField>,
                  setState: (state: Boolean, savedFilledForm: FilledForm?) -> Unit){
        val writeBatch = db.batch()
        val filledFormDoc: DocumentReference = if (filledForm.id.isEmpty() ){
            db.collection(FILLED_FORMS_COLL).document()
        }else{
            db.collection(FILLED_FORMS_COLL).document(filledForm.id)
        }

        writeBatch.set(filledFormDoc, filledForm, SetOptions.merge())

        for (filledField in filledFields){
            val filledFieldDoc: DocumentReference = if (filledForm.id.isEmpty() ){
                filledFormDoc.collection(FILLED_FIELDS_COLL).document()
            }else{
                filledFormDoc.collection(FILLED_FIELDS_COLL).document(filledField.id)
            }

            writeBatch.set(filledFieldDoc, filledField, SetOptions.merge())
        }
        writeBatch
            .commit()
            .addOnSuccessListener {
                val savedFilledForm = filledForm.copy(id = filledFormDoc.id)
                setState(true, savedFilledForm)
            }
            .addOnFailureListener {
                Timber.e(it)
                setState(false, null)
            }
    }


    private fun fieldToFilledField(field: Field) = with(field){
        val answers = getAnswersAsObject()
        //se inicializa active en false por ser un formulario 'nuevo'. Se usa para mostrar el mesaje de campo requerido
        FilledField(id,answers,"", fieldType, order, required, description, false)
    }

    private fun formToFilledForm(form: Form) = with(form){
        FilledForm("", creatorName, id, null, user?.uid!!, description, false, active = true)
    }
}