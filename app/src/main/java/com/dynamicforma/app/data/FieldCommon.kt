package com.dynamicforma.app.data

interface FieldCommon : BasicProperties{
    var fieldType: Int
    var order: Int
    var required: Boolean
}