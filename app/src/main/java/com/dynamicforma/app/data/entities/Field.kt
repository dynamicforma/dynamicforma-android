package com.dynamicforma.app.data.entities

import com.dynamicforma.app.data.FieldCommon
import com.google.firebase.firestore.DocumentId
import com.dynamicforma.app.data.FieldTypeEnum.SimpleSelect
import com.dynamicforma.app.data.FieldTypeEnum.SimpleText
import com.dynamicforma.app.data.FieldTypeEnum.CheckGroup
import com.dynamicforma.app.data.FieldTypeEnum.DataPicker
import com.dynamicforma.app.data.FieldTypeEnum.RadioGroup
import com.dynamicforma.app.R


data class Field(
    @DocumentId override val id: String = "",
    override var description: String = "",
    var answers: List<String> = listOf(),
    override var fieldType: Int = 0,
    override var order: Int = 0,
    override var required: Boolean = false,
    override var active: Boolean = false
) : FieldCommon {
    init {
        answers.map {

        }
    }

    fun getLayoutRes() = when(this.fieldType) {
        SimpleText.ordinal -> R.layout.simple_text
        SimpleSelect.ordinal -> R.layout.simple_select
        RadioGroup.ordinal, CheckGroup.ordinal -> R.layout.check_radio_group
        DataPicker.ordinal -> R.layout.date_picker
        else -> 0
    }

    fun getAnswersAsObject(): List<Answer> = answers.mapIndexed(this::createAnswerObject)


    private fun createAnswerObject(index: Int, ans: String) = Answer(id = index.toString(), description = ans, initialChosen = false)
}