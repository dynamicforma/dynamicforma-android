package com.dynamicforma.app

import com.dynamicforma.app.data.entities.FilledForm

data class EventFormModified(val isFormModified: Boolean)

data class EventFormSaved(val form: FilledForm)