
object Versions {
    const val androidBuildToolsVersion = "29.0.3"
    const val androidMinSdkVersion = 21
    const val androidTargetSdkVersion = 29
    const val androidCompileSdkVersion = 29
    const val kotlinVersion = "1.4.10"

    const val constraintLayoutVersion = "2.0.4"
    const val rxKotlinVersion = "2.4.0"
    const val rxAndroidVersion = "2.1.1"
    const val rxJavaVersion = "2.2.15"
    const val firebaseAuthVersion = "20.0.0"
    const val firestoreVersion = "22.0.0"
    const val firebaseCoreVersion = "18.0.0"
    const val hiltVersion = "2.29-alpha"
    const val hiltViewModelVersion = "1.0.0-alpha02"
    const val hiltCompiler = "1.0.0-alpha02"
    const val legacySupportVersion = "1.0.0"
    const val preferencesVersion = "1.1.1"
    const val navigationUiVersion = "2.3.1"
    const val navigationFragmentVersion = "2.3.1"
    const val androidLifecycleVersion = "2.2.0"
    const val pagingVersion = "2.1.2"
    const val materialVersion = "1.2.0-alpha01"
    const val androidCoreVersion = "1.3.1"
    const val androidCompatVersion = "1.2.0"
    const val timberVersion = "4.7.1"

    const val junitVersion = "4.13.1"
    const val espressoVersion = "3.3.0"
    const val androidTestRunnerVersion = "1.1.0"
}

object Dependencies {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlinVersion}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayoutVersion}"
    const val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJavaVersion}"
    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroidVersion}"
    const val rxKotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlinVersion}"
    const val timber = "com.jakewharton.timber:timber:${Versions.timberVersion}"
    const val firebaseAuth = "com.google.firebase:firebase-auth:${Versions.firebaseAuthVersion}"
    const val firestore = "com.google.firebase:firebase-firestore-ktx:${Versions.firestoreVersion}"
    const val firebaseCore = "com.google.firebase:firebase-core:${Versions.firebaseCoreVersion}"
    const val hilt = "com.google.dagger:hilt-android:${Versions.hiltVersion}"
    const val hiltViewModel = "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hiltViewModelVersion}"
    const val hiltCompiler = "androidx.hilt:hilt-compiler:${Versions.hiltCompiler}"
    const val hiltAndroidCompiler = "com.google.dagger:hilt-android-compiler:${Versions.hiltVersion}"
    const val legacySupport = "androidx.legacy:legacy-support-v4:${Versions.legacySupportVersion}"
    const val preferences = "androidx.preference:preference-ktx:${Versions.preferencesVersion}"
    const val navigationUi = "androidx.navigation:navigation-ui-ktx:${Versions.navigationUiVersion}"
    const val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigationFragmentVersion}"
    const val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.androidLifecycleVersion}"
    const val lifecycleLiveData = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.androidLifecycleVersion}"
    const val lifecycleReactivestreams = "androidx.lifecycle:lifecycle-reactivestreams-ktx:${Versions.androidLifecycleVersion}"
    const val lifecycleCommon = "androidx.lifecycle:lifecycle-common-java8:${Versions.androidLifecycleVersion}"
    const val lifecycleExt = "androidx.lifecycle:lifecycle-extensions:${Versions.androidLifecycleVersion}"
    const val paging = "androidx.paging:paging-runtime-ktx:${Versions.pagingVersion}"
    const val material = "com.google.android.material:material:${Versions.materialVersion}"
    const val androidCore = "androidx.core:core-ktx:${Versions.androidCoreVersion}"
    const val androidCompat = "androidx.appcompat:appcompat:${Versions.androidCompatVersion}"

    const val junit = "junit:junit:${Versions.junitVersion}"
    const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoVersion}"
    const val androidTestRunner = "androidx.test:runner:${Versions.androidTestRunnerVersion}"

}